# Serenity Valley

![(https://vignette.wikia.nocookie.net/firefly/images/0/03/Serenity_Valley.jpg/revision/latest/scale-to-width-down/250?cb=20080316230202)](https://vignette.wikia.nocookie.net/firefly/images/0/03/Serenity_Valley.jpg/revision/latest?cb=20080316230202)

Serenity Valley

**Serenity Valley**  was a valley located on  [Hera](https://firefly.fandom.com/wiki/Hera "Hera"); it was mainly sparse and rocky with little vegetation. The valley was famous for being the location of the  [Battle of Serenity Valley](https://firefly.fandom.com/wiki/Battle_of_Serenity_Valley "Battle of Serenity Valley")—one of the bloodiest battles of the entire  [Unification War](https://firefly.fandom.com/wiki/Unification_War "Unification War"). Due to Hera's strategic position, taking the planet was a key to winning the war, and Serenity Valley became the turning point of the entire conflict.

The war devastated Serenity Valley, mainly due to the extreme  [Union of Allied Planets](https://firefly.fandom.com/wiki/Union_of_Allied_Planets "Union of Allied Planets")  bombardment of the area at the end of the battle. Seven years on, the valley was still blackened and charred by the fire storm that swept through it, the only landmark being a  [graveyard](https://firefly.fandom.com/wiki/Serenity_graveyard "Serenity graveyard")  on the hills next to the valley. Over half a million men and women, Alliance and  [Independent](https://firefly.fandom.com/wiki/Independent_Planets "Independent Planets")  alike, were buried in the graveyard, each with his or her own small, identical headstone. Some had names; most didn't.

The graveyard was located on the opposite side of the valley from the town of  [Serenity View](https://firefly.fandom.com/wiki/Serenity_View "Serenity View"). Families and friends of the fallen come to Hera to visit the graves, which bloom with flowers, photos, and mementos. Even the unmarked graves had their share; plenty of families never saw their children return, and many have picked an unnamed grave and honored it, hoping someone else was doing the same for their son or daughter.
  